﻿using Connectors.SDK.Extraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace TestConnector
{
    public class TestAdapter : IMetadataProvider, IDataProvider
    {
        #region Private Fields
        // Fields for Time dropdown
        private const string Time = "Time";
        private const string TimeField = "TimeField";
        private const string Days = "Days";
        private const int MILLISECONDS_IN_DAY = 86400000;
        private const int MILLISECONDS_IN_HOUR = 3600000;

        // Fields for Transaction dropdown
        private const string TransactionField = "TestTransaction";
        private const string PeriodField = "Period";
        private const string Transactions = "Transaction";

        // Column names for generating transactions
        private const string AccountField = "Account";
        private const string EntityField = "Entity";
        private const string ScenarioField = "Scenario";
        private const string DepartmentField = "Department";
        private const string Value1 = "Value1";
        private const string Value2 = "Value2";
        private const string Value3 = "Value3";

        // Arrays of column data
        int[] Account = { 40010, 40020, 99999 };
        string[] Entity = { "SUS", "SAS", "SEM" };
        string[] Scenario = { "BUD", "ACT", "OBA" };
        int[] Department = { 100, 200, 300 };

        static Random rnd = new Random();

        private TestCredentials cred;
        private bool _authenticated;
        #endregion Private Fields

        public TestAdapter (TestCredentials login)
        {
            this.cred = login;
        }

        #region Public Methods


        public void Authenticate()
        {
            if (cred.transactionLimit > 16000000)
            {
                throw new Exception("Cannot generate data with more than 16 million");
            }
            this._authenticated = true;
        }

        public IEnumerable<IFieldDescriptor> DescribeForeignObject(IForeignObjectToken foreignObjectToken)
        {
            if (!this._authenticated)
            {
                throw new Connectors.SDK.Exceptions.UnauthorizedException();
            }

            switch ((string)foreignObjectToken.Token)
            {
                case Transactions:
                    return new List<IFieldDescriptor>
                    {
                        new FieldDescriptor(TransactionField,typeof(int)),
                    };
                case Time:
                    return new List<IFieldDescriptor>
                    {
                        new FieldDescriptor(TimeField,typeof(int)),
                    };
                default:
                    throw new ArgumentException($"Unknown token {(string)foreignObjectToken.Token}");
            }
        }

        public void Dispose()
        {
            return;
        }

        public DataTable GenerateTransactionTable(int limit)
        {
            DataTable testTable = new DataTable();
            DataColumn transactionIdField = new DataColumn();
            transactionIdField.ColumnName = "Transaction Id";
            long transactionId = 1;
            testTable.Columns.Add(transactionIdField);
            testTable.Columns.Add(AccountField);
            testTable.Columns.Add(ScenarioField);
            testTable.Columns.Add(EntityField);
            testTable.Columns.Add(DepartmentField);
            testTable.Columns.Add(PeriodField);
            testTable.Columns.Add(Value1);
            testTable.Columns.Add(Value2);
            testTable.Columns.Add(Value3);

            for (int i = 0; i < limit; i++)
            {
                DataRow tempRow = testTable.NewRow();
                tempRow[transactionIdField] = transactionId;
                tempRow[AccountField] = Account[rnd.Next(Account.Count())];
                tempRow[ScenarioField] = Scenario[rnd.Next(Scenario.Count())];
                tempRow[EntityField] = Entity[rnd.Next(Entity.Count())];
                tempRow[DepartmentField] = Department[rnd.Next(Department.Count())];
                tempRow[PeriodField] = cred.PeriodValue;
                tempRow[Value1] = rnd.Next(1, 123456789);
                tempRow[Value2] = rnd.NextDouble();
                tempRow[Value3] = rnd.Next(1, 123456789);


                testTable.Rows.Add(tempRow);
                transactionId++;
            }

            return testTable;
        }

        public async Task Runtime(int hours, int days = 0)
        {
            int calculatedTime = (hours * MILLISECONDS_IN_HOUR) + (days * MILLISECONDS_IN_DAY);

            await Task.Delay(calculatedTime);
        }

        public DataTable Extract(IForeignObjectToken foreignObjectToken, IProjection projection)
        {
            if (!this._authenticated)
            {
                throw new Connectors.SDK.Exceptions.UnauthorizedException();
            }

            int computedLimit = projection.Limit.HasValue ? projection.Limit.Value : cred.transactionLimit;

            switch ((string)foreignObjectToken.Token)
            {
                case Transactions:
                    {
                        var table = new DataTable();
                        table.MinimumCapacity = computedLimit;
                        table = GenerateTransactionTable(computedLimit);

                        return table;
                    }
                case Time:
                    {
                        var noData = new DataTable();
                        DataRow complete = noData.NewRow();
                        DataColumn column = new DataColumn();
                        column.DataType = System.Type.GetType("System.UInt64");
                        column.ColumnName = "Done";
                        noData.Columns.Add(column);

                        Runtime(cred.hourTime, cred.dayTime).GetAwaiter().GetResult();

                        complete["Done"] = 1;
                        noData.Rows.Add(complete);
                        return noData;
                    }
                default:
                    throw new ArgumentException($"Unknown token {(string)foreignObjectToken.Token}");
            }
        }

        public IEnumerable<IForeignObjectToken> ReadForeignObjects()
        {
            if (!this._authenticated)
            {
                throw new Connectors.SDK.Exceptions.UnauthorizedException();
            }

            return new List<IForeignObjectToken>
            {
                new ForeignObjectToken(Transactions),
                new ForeignObjectToken(Time),
            };
        }
        #endregion Public Methods
    }
}
