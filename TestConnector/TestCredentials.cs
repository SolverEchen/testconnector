﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Connectors.SDK.Attributes.Authentication;

namespace TestConnector
{
    [AuthenticationToken]
    public class TestCredentials
    {
        #region Public Properties
        [Required]
        [Label("Transactions to generate: ")]
        public int transactionLimit { get; set; }
        [Required]
        [Label("Period to generate: ")]
        public int PeriodValue { get; set; }
        [Required]
        [Label("Days to run: ")]
        public int dayTime { get; set; }
        [Required]
        [Label("Hours to run: ")]
        public int hourTime { get; set; }
        #endregion Public Propertries

        #region Public Constructor
        public TestCredentials (int transactionInput, int hourInput, int periodInput, int dayInput)
        {
            this.transactionLimit = transactionInput;
            this.dayTime = dayInput;
            this.hourTime = hourInput;
            this.PeriodValue = periodInput;
        }
        public TestCredentials() {}
        #endregion Public Constructor
    }
}
